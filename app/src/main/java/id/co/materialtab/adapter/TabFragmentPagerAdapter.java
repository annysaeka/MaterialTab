package id.co.materialtab.adapter;


import id.co.materialtab.fragment.Tab1Fragment;
import id.co.materialtab.fragment.Tab2Fragment;
import id.co.materialtab.fragment.Tab3Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

/**
 * Created by Annysa Eka on 3/7/2017.
 */
public class TabFragmentPagerAdapter extends FragmentPagerAdapter  {
    //nama tab nya
    String[] title = new String[]{
            "TUGAS 1", "TUGAS 2", "TUGAS 3"
    };

    public TabFragmentPagerAdapter(FragmentManager fm) {

        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            case 2:
                fragment = new Tab3Fragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    public int getCount() {
        return title.length;
    }

}
